FROM amazoncorretto:11
EXPOSE 9001
ADD target/cloud-config-server.jar cloud-config-server.jar
ENTRYPOINT ["java","-jar","/cloud-config-server.jar"]